<?php

include('../parts/global/php/sessioncheck.php');

$errors         = array();  	// array to hold validation errors
$data 			= array(); 		// array to pass back data

// validate the variables ======================================================
	// if any of these variables don't exist, add an error to our $errors array

	if (empty($_POST['email']))
		$errors['email'] = 'Email is required.';

	if (empty($_POST['password']))
		$errors['password'] = 'Password is required.';

// return a response ===========================================================

	// if there are any errors in our errors array, return a success boolean of false
	if ( ! empty($errors)) {

		// if there are items in our errors array, return those errors
		$data['success'] = false;
		$data['errors']  = $errors;
	} else {

		// if there are no errors process our form, then return a message

		// DO ALL YOUR FORM PROCESSING HERE
		// THIS CAN BE WHATEVER YOU WANT TO DO (LOGIN, SAVE, UPDATE, WHATEVER)

		include('../parts/connect/connectusers.php');
		$email = $_POST['email'];
		$ip = $_SERVER['REMOTE_ADDR'];
		$password  = $_POST['password'];
		$password = crypt($password,'$6$rounds=50000$io3u4n80*^(nyoG');
		$password = substr($password, strrpos($password, '$') + 1);
		$sql = "SELECT * FROM users WHERE email = '".$email."' AND password = '".$password."'";

        $result = $users->query($sql);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {

				$guid = rtrim(base64_encode(md5(microtime())),"==");

					setcookie('guid', $guid, time() + (86400 * 30), "/");
					setcookie('uid', $row['id'], time() + (86400 * 30), "/");

					$sqlb = "UPDATE users SET `guid` = '".$guid."', `ip` = '".$ip."', `login` = '".date('Y-m-d H:i:s')."' WHERE email = '".$email."'";
			        $resultb = $users->query($sqlb);
			        $resultb;

				// show a message of success and provide a true success variable
				$data['success'] = true;
				$data['message'] = 'Logging in, please wait.';

            }
        } else {
			$data['success'] = false;
			$errors['email'] = '';
			$errors['password'] = '';
			$data['errors']  = $errors;
			$data['message'] = 'The details you have entered are not correct please check and try again!';
        }


	}

	// return all our data to an AJAX call
	echo json_encode($data);