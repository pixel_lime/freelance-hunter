<?php

	// Page Settings
	$title = 'Login to Freelance Hunter';
	$description = 'Login to Freelance Hunter to start receiving work';

	// Style Sheets
	$bootstrapcss = '1';
	$metisMenucss = '1';
	$bootstrap_socialcss = '1';
	$sb_admin_2css = '1';
	$font_awesomecss = '1';
	$dataTablescss;
	$morriscss;
	$timelinecss;

	// Javascript
	$jqueryjs = '1';
	$bootstrapjs = '1';
	$metisMenujs = '1';
	$sb_admin_2js = '1';

	// Menus
	$topmenu = '1';
	$sidemenu;

	// Global Settings
	include('../parts/global/php/globalsettings.php');

?>
<?php include('../parts/global/php/sessioncheck.php'); ?>
<?php include('../parts/framework/header.php'); ?>
	<?php include('../parts/global/php/menu.php'); ?>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Sign In</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <a class="btn btn-block btn-social btn-facebook">
                                <i class="fa fa-facebook"></i> Sign in with Facebook
                            </a>
                        </div>
                    <hr/>
                        <form role="form" method="post">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="E-mail" name="email" type="email" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me">Remember Me
                                    </label>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <button class="btn btn-lg btn-success btn-block">Login</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php include('../parts/framework/footer.php'); ?>
