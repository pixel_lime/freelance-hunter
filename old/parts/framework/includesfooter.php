    <?php if(isset($jqueryjs)) : ?><script src="../bower_components/jquery/dist/jquery.min.js"></script><?php endif; ?>
    <?php if(isset($bootstrapjs)) : ?><script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script><?php endif; ?>
    <?php if(isset($metisMenujs)) : ?><script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script><?php endif; ?>
    <?php if(isset($sb_admin_2js)) : ?><script src="../dist/js/sb-admin-2.js"></script><?php endif; ?>
    <?php if(isset($datatablesjs)) : ?>
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>
	<?php endif; ?>