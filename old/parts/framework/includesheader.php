    <?php if(isset($bootstrapcss)) : ?><link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"><?php endif; ?>
    <?php if(isset($metisMenucss)) : ?><link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet"><?php endif; ?>
    <?php if(isset($bootstrap_socialcss)) : ?><link href="../bower_components/bootstrap-social/bootstrap-social.css" rel="stylesheet"><?php endif; ?>
    <?php if(isset($sb_admin_2css)) : ?><link href="../dist/css/sb-admin-2.css" rel="stylesheet"><?php endif; ?>
    <?php if(isset($font_awesomecss)) : ?><link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><?php endif; ?>
    <?php if(isset($dataTablescss)) : ?><link href="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet"><?php endif; ?>
    <?php if(isset($dataTablescss)) : ?><link href="../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet"><?php endif; ?>
    <?php if(isset($morriscss)) : ?><link href="../bower_components/morrisjs/morris.css" rel="stylesheet"><?php endif; ?>
    <?php if(isset($timelinecss)) : ?><link href="../css/timeline.css" rel="stylesheet"><?php endif; ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->