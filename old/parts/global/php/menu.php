        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <?php if(isset($topmenu)) : include('../parts/global/php/menutop.php'); endif; ?>
            <?php if(isset($sidemenu)) : include('../parts/global/php/menuside.php'); endif; ?>
        </nav>