        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <?php if(isset($topmenu)) : include_once('menutop.php'); endif; ?>
            <?php if(isset($sidemenu)) : include_once('menuside.php'); endif; ?>
        </nav>
