            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="/dashboard/"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="/projects/"><i class="fa fa-code fa-fw"></i> Projects</a>
                        </li>
                        <li>
                            <a href="/users/"><i class="fa fa-users fa-fw"></i> Users</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-files-o fa-fw"></i> Settings<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li>
                                    <a href="/settings/"><i class="fa fa-code fa-fw"></i> Your Account</a>
                                </li>
                                <li>
                                    <a href="/your-projects/"><i class="fa fa-code fa-fw"></i> Your Projects</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                   </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
