<?php

	// Page Settings
	$title = 'Latest Available Projects - Freelance Hunter';
	$description = 'Here you can view and bid on the latest projects.';

	// Style Sheets
	$bootstrapcss = '1';
	$metisMenucss = '1';
	$bootstrap_socialcss = '1';
	$sb_admin_2css = '1';
	$font_awesomecss = '1';
	$dataTablescss;
	$morriscss;
	$timelinecss;

	// Javascript
	$jqueryjs = '1';
	$bootstrapjs = '1';
	$metisMenujs = '1';
	$sb_admin_2js = '1';
    $datatablesjs = '1';
    $ajax = '1';
    $projectjs = '1';

	// Menus
	$topmenu = '1';
	$sidemenu = '1';

    // MySQLi
    $connectbids = '1';
    $connectprojects = '1';
    $connectsystem;
    $connectusers = '1';

	// Global Settings
	include('../parts/global/php/globalsettings.php');

?>
<?php include('../parts/global/php/sessioncheck.php'); ?>
<?php include('../parts/framework/header.php'); ?>
<?php include('../parts/global/php/menu.php'); ?>

<?php
$_SERVER['REQUEST_URI_PATH'] = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$id = explode('/', rtrim($_SERVER['REQUEST_URI_PATH'], '/'));
$id = $id[2];
$sql = "SELECT * FROM project WHERE id = '$id'";
echo $sql;
$result = $project->query($sql);
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
    $ptitle = $row['title'];
    $pcontent = $row['content'];
    $pcategories = $row['categories'];
    $pmaxprice = $row['maxprice'];
    $pminprice = $row['minprice'];
    $pbids = $row['bids'];
    }
}
?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?= $ptitle ?></h1>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Project Details
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-pills">
                                <li class="active"><a href="#details-pills" data-toggle="tab">Details</a>
                                </li>
                                <li><a href="#seller-profile-pills" data-toggle="tab">Seller Profile</a>
                                </li>
                                <li><a href="#bids-pills" data-toggle="tab">Bids (4)</a>
                                </li>
                                <li><a href="#bid-now-pills" data-toggle="tab">Bid Now</a>
                                </li>
                                <li><a href="#report-pills" data-toggle="tab">Report</a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="details-pills">
                                    <h3>Details:-</h3>
                                    <h4><?= $ptitle ?></h4>
                                    <p>ID: <?= $id; ?></p>
                                    <h4>Content:-</h4>
                                    <p><?= $pcontent ?></p>
                                    <h4>Value</h4>
                                    <p><?php if($pmaxprice != '0') : ?>&euro;<?= $pminprice ?> - &euro;<?= $pmaxprice ?><?php else : ?>&euro;<?= $pminprice ?><?php endif; ?></p>
                                    <h4>Categories:-</h4>
                                    <p><?= $pcategories ?></p>
                                </div>
                                <div class="tab-pane fade" id="seller-profile-pills">
                                    <h4>Profile Tab</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>
                                <div class="tab-pane fade" id="bids-pills">
                                    <h4>Bids</h4>


                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Bidder</th>
                                                    <th>Delivery Time</th>
                                                    <th>Bid Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            <?php
                                            $i = '0';
                                            $bid = '0';
                                            $sql = "SELECT * FROM bids WHERE project_id = '$id'";
                                            $result = $bids->query($sql);
                                            if ($result->num_rows > 0) {
                                                while($row = $result->fetch_assoc()) {
                                                ?>
                                                <tr class="even gradeC">
                                                    <td><?= ++$i; ?></td>
                                                    <td><a href="/users/<?= $row['user_id']; ?>/" title=""><?= $row['user_id']; ?></a></td>
                                                    <td><?= $row['delivery_time']; ?> Days</td>
                                                    <td>&euro;<?= $row['bid']; ?><?php $bid = $bid + $row['bid']; ?></td>
                                                </tr>
                                            <?php
                                                }
                                            } else { ?>
                                                <tr>
                                                    <td colspan="4">0 bids, be the first.</td>
                                                </tr>
                                                
                                            <?php
                                            }
                                            ?>
                                            <?php $medium = round($bid/$i); ?> 
                                            </tbody>
                                        </table>
                                    </div>


                                </div>
                                <div class="tab-pane fade" id="bid-now-pills">
                                <h4>Bid Now</h4>
                                        
                                <div class="row">
                                    <div class="col-lg-6">
                                        <form action="../project.php" id="placebid" role="form" method="post">

                                            <div id="titles-group" class="form-group">
                                                <label>Title </label>
                                                <input placeholder="Bid Title (min of 25 char)" id="titles" name="titles" class="form-control">
                                                <input type="hidden" id="url" name="url" class="form-control" value="<?= $_SERVER['REQUEST_URI']; ?>">
                                            </div>
                                            <div id="proposal-group" class="form-group">
                                                <label>Proposal</label>
                                                <textarea placeholder="Please ensure you have read the project description properly.  (min of 100 char)" id="proposal" name="proposal" rows="6" class="form-control"></textarea>
                                            </div>
                                            <label>Required Time</label>
                                            <div id="rtime-group" class="form-group input-group">
                                                <input placeholder="Required Time" id="rtime" name="rtime" class="form-control">
                                                <span class="input-group-addon">Days</span>
                                            </div>
                                            <label>Bid (in euro's)</label>
                                            <div id="bid-group" class="form-group input-group">
                                                <span class="input-group-addon"><i class="fa fa-eur"></i>
                                                </span>
                                                <input type="text" placeholder="Average bid &euro;<?= $medium; ?>" id="bid" name="bid" class="form-control">
                                            </div>
                                            <div id="file-group" class="form-group">
                                                <label>Attachment</label>
                                                <input type="file" id="file" name="file">
                                            </div>

                                            <button class="btn btn-lg btn-success btn-block">Submit Bid</button>
                                        </form>
                                    </div>
                                    <!-- /.col-lg-6 (nested) -->
                                    <div class="col-lg-6">
                                        <h4><?= $ptitle ?></h4>
                                        <p>Average bid &euro;<?= $medium; ?></p>
                                        <p><?= $pcontent ?></p>
                                    </div>
                                    <!-- /.col-lg-6 (nested) -->
                                </div>

                                </div>
                                <div class="tab-pane fade" id="report-pills">
                                    <h4>Report Project</h4>
                                    <form action="/report/" id="report" role="form" method="post">
                                        <div id="reason-group" class="form-group">
                                            <label>Reason</label>
                                            <textarea placeholder="Please ensure you correctly state the reason." id="reason" name="reason" rows="6" class="form-control"></textarea>
                                        </div>
                                        <input type="hidden" value="<?= $id ?>" id="id" name="id">
                                        <button style class="btn btn-lg btn-danger btn-block">Report Project</button>
                                    </form>
                                    <p></p>
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
            </div>
        </div>

<?php include('../parts/framework/footer.php'); ?>
