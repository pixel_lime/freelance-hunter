// magic.js
$(document).ready(function() {

	// process the form
	$('#placebid').submit(function(event) {

		$('.form-group').removeClass('has-error'); // remove the error class
		$('.help-block').remove(); // remove the error text

		// get the form data
		// there are many ways to get this data using jQuery (you can use the class or id also)
		var formData = {
			'titles' 			: $('input[name=titles]').val(),
			'proposal' 			: $('input[name=proposal]').val(),
			'rtime' 			: $('input[name=rtime]').val(),
			'bid' 				: $('input[name=bid]').val(),
		};

		// process the form
		$.ajax({
			type 		: 'POST', // define the type of HTTP verb we want to use (POST for our form)
			url 		: '../project.php', // the url where we want to POST
			data 		: formData, // our data object
			dataType 	: 'json', // what type of data do we expect back from the server
			encode 		: true
		})
			// using the done promise callback
			.done(function(data) {

				// here we will handle errors and validation messages
				if ( ! data.success) {
					
					// handle errors for username ---------------
					if (data.errors.titles) {
						$('#titles-group').addClass('has-error'); // add the error class to show red input
						$('#titles-group').append('<div class="help-block">' + data.errors.titles + '</div>'); // add the actual error message under our input
					}
					
					// handle errors for proposal ---------------
					if (data.errors.proposal) {
						$('#proposal-group').addClass('has-error'); // add the error class to show red input
						$('#proposal-group').append('<div class="help-block">' + data.errors.proposal + '</div>'); // add the actual error message under our input
					}

					// handle errors for rtime ---------------
					if (data.errors.rtime) {
						$('#rtime-group').addClass('has-error'); // add the error class to show red input
						$('#rtime-group').append('<div class="help-block">' + data.errors.rtime + '</div>'); // add the actual error message under our input
					}

					// handle errors for name ---------------
					if (data.errors.bid) {
						$('#bid-group').addClass('has-error'); // add the error class to show red input
						$('#bid-group').append('<div class="help-block">' + data.errors.bid + '</div>'); // add the actual error message under our input
					}

					if (data.message) {
						$('#proposal-group').addClass('has-error'); // add the error class to show red input
						$('#rtime-group').addClass('has-error'); // add the error class to show red input
						$('#bid-group').addClass('has-error'); // add the error class to show red input
						$('#titles-group').addClass('has-error');
						setTimeout(function() {$('.alert-danger').hide()}, 3000);
						$('#placebid').append('<div style="margin-top:20px;" class="alert alert-danger">' + data.message + '</div>');
					}

				} else {
					// ALL GOOD! just show the success message!
					$('#placebid').append('<div style="margin-top:20px;" class="alert alert-success">' + data.message + '</div>');
				}
			})

			// using the fail promise callback
			.fail(function(data) {
			});

		// stop the form from submitting the normal way and refreshing the page
		event.preventDefault();
	});

});
