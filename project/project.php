<?php
include('../parts/connect/connectusers.php');

include('../parts/global/php/sessioncheck.php');

$errors         = array();  	// array to hold validation errors
$data 			= array(); 		// array to pass back data

// validate the variables ======================================================
	// if any of these variables don't exist, add an error to our $errors array

	if (empty($_POST['titles']))
		$errors['titles'] = 'Title is required.';

	if (!empty($_POST['titles']) && strlen($_POST['titles']) < '25')
		$errors['titles'] = 'Title length is too short.';

	if (empty($_POST['proposal']))
		$errors['proposal'] = 'Proposal is required.';

	if (!empty($_POST['proposal']) && strlen($_POST['proposal']) < '100')
		$errors['proposal'] = 'Proposal length is too short.';

	if (empty($_POST['rtime']))
		$errors['rtime'] = 'Required Time is required.';

	if (empty($_POST['bid']))
		$errors['bid'] = 'bid is required.';

	if (!empty($_POST['bid']) && $_POST['bid'] < '2')
		$errors['bid'] = 'bid is too low.';


// return a response ===========================================================

	// if there are any errors in our errors array, return a success boolean of false
	if ( ! empty($errors)) {

		// if there are items in our errors array, return those errors
		$data['success'] = false;
		$data['errors']  = $errors;
	} else {

		// if there are no errors process our form, then return a message

		// DO ALL YOUR FORM PROCESSING HERE
		// THIS CAN BE WHATEVER YOU WANT TO DO (LOGIN, SAVE, UPDATE, WHATEVER)


	}

	// return all our data to an AJAX call
	echo json_encode($data);