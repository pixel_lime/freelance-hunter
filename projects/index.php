<?php

	// Page Settings
	$title = 'Latest Available Projects - Freelance Hunter';
	$description = 'Here you can view and bid on the latest projects.';

	// Style Sheets
	$bootstrapcss = '1';
	$metisMenucss = '1';
	$bootstrap_socialcss = '1';
	$sb_admin_2css = '1';
	$font_awesomecss = '1';
	$dataTablescss;
	$morriscss;
	$timelinecss;

	// Javascript
	$jqueryjs = '1';
	$bootstrapjs = '1';
	$metisMenujs = '1';
	$sb_admin_2js = '1';
    $datatablesjs = '1';

	// Menus
	$topmenu = '1';
	$sidemenu = '1';

    // MySQLi
    $connectbids;
    $connectprojects = '1';
    $connectsystem;
    $connectusers;

	// Global Settings
	include('../parts/global/php/globalsettings.php');

?>
<?php include('../parts/global/php/sessioncheck.php'); ?>
<?php include('../parts/framework/header.php'); ?>
<?php include('../parts/global/php/menu.php'); ?>


        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Projects</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Latest Projects
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Description</th>
                                            <th>Categories</th>
                                            <th>Price</th>
                                            <th>Bids</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $sql = 'SELECT * FROM project';
                                    $result = $project->query($sql);
                                    if ($result->num_rows > 0) {
                                        while($row = $result->fetch_assoc()) {
                                        ?>
                                        <tr class="even gradeC">
                                            <td><a href="/project/<?= $row['id'] ?>/" title="<?= $row['title'] ?>"><?= $row['title'] ?></a></td>
                                            <td><?= $row['content'] ?></td>
                                            <td><?= $row['categories'] ?></td>
                                            <td class="center"><?php if($row['maxprice'] != '0') : ?>&euro;<?= $row['minprice'] ?> - &euro;<?= $row['maxprice'] ?><?php else : ?>&euro;<?= $row['minprice'] ?><?php endif; ?></td>
                                            <td class="center"><?= $row['bids'] ?></td>
                                        </tr>
                                    <?php
                                        }
                                    } else {
                                        echo "0 results";
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<?php include('../parts/framework/footer.php'); ?>
