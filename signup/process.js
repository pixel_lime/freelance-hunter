// magic.js
$(document).ready(function() {

	// process the form
	$('form').submit(function(event) {

		$('.form-group').removeClass('has-error'); // remove the error class
		$('.help-block').remove(); // remove the error text

		// get the form data
		// there are many ways to get this data using jQuery (you can use the class or id also)
		var formData = {
			'username' 			: $('input[name=username]').val(),
			'email' 			: $('input[name=email]').val(),
			'password' 				: $('input[name=password]').val(),
		};

		// process the form
		$.ajax({
			type 		: 'POST', // define the type of HTTP verb we want to use (POST for our form)
			url 		: 'signup.php', // the url where we want to POST
			data 		: formData, // our data object
			dataType 	: 'json', // what type of data do we expect back from the server
			encode 		: true
		})
			// using the done promise callback
			.done(function(data) {

				// here we will handle errors and validation messages
				if ( ! data.success) {
					
					// handle errors for username ---------------
					if (data.errors.username) {
						$('#username-group').addClass('has-error'); // add the error class to show red input
						$('#username-group').append('<div class="help-block">' + data.errors.username + '</div>'); // add the actual error message under our input
					}
					
					// handle errors for email ---------------
					if (data.errors.email) {
						$('#email-group').addClass('has-error'); // add the error class to show red input
						$('#email-group').append('<div class="help-block">' + data.errors.email + '</div>'); // add the actual error message under our input
					}

					// handle errors for name ---------------
					if (data.errors.password) {
						$('#password-group').addClass('has-error'); // add the error class to show red input
						$('#password-group').append('<div class="help-block">' + data.errors.password + '</div>'); // add the actual error message under our input
					}

					if (data.message) {
						$('#username-group').addClass('has-error'); // add the error class to show red input
						$('#email-group').addClass('has-error'); // add the error class to show red input
						$('#password-group').addClass('has-error'); // add the error class to show red input
						setTimeout(function() {$('.alert-danger').hide()}, 3000);
						$('form').append('<div style="margin-top:20px;" class="alert alert-danger">' + data.message + '</div>');
					}

				} else {

					// ALL GOOD! just show the success message!
					$('form').append('<div style="margin-top:20px;" class="alert alert-success">' + data.message + '</div>');

					// usually after form submission, you'll want to redirect
					setTimeout(function() {window.location = '/dashboard/'}, 2000); // redirect a user to another page

				}
			})

			// using the fail promise callback
			.fail(function(data) {
			});

		// stop the form from submitting the normal way and refreshing the page
		event.preventDefault();
	});

});
