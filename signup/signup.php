<?php
include('../parts/connect/connectusers.php');

include('../parts/global/php/sessioncheck.php');

$username = strtolower($_POST['username']);
$email = $_POST['email'];
$ip = $_SERVER['REMOTE_ADDR'];
$password  = $_POST['password'];
$password = crypt($password,'$6$rounds=50000$io3u4n80*^(nyoG');
$password = substr($password, strrpos($password, '$') + 1);

$errors         = array();  	// array to hold validation errors
$data 			= array(); 		// array to pass back data

// validate the variables ======================================================
	// if any of these variables don't exist, add an error to our $errors array

	if (empty($_POST['username']))
		$errors['username'] = 'Username is required.';

	$sql = "SELECT * FROM users WHERE username = '".$username."'";

    $usercheck = $users->query($sql);
    if ($usercheck->num_rows > 0) {
		$errors['username'] = 'Username is taken.<br/>Alternitive username:- <br/>'.$username.'_'.rand(0,9).', '.$username.'_'.rand(10,19);
    }

	$sql = "SELECT * FROM users WHERE email = '".$email."'";

    $emailcheck = $users->query($sql);
    if ($emailcheck->num_rows > 0) {
		$errors['email'] = 'Email address is already registered.';
    }

	if (empty($_POST['email']))
		$errors['email'] = 'Email is required.';

	if (empty($_POST['password']))
		$errors['password'] = 'Password is required.';
	if (strlen($_POST['password']) < '8')
		$errors['password'] = 'Password length is too short.';

// return a response ===========================================================

	// if there are any errors in our errors array, return a success boolean of false
	if ( ! empty($errors)) {

		// if there are items in our errors array, return those errors
		$data['success'] = false;
		$data['errors']  = $errors;
	} else {

		// if there are no errors process our form, then return a message

		// DO ALL YOUR FORM PROCESSING HERE
		// THIS CAN BE WHATEVER YOU WANT TO DO (LOGIN, SAVE, UPDATE, WHATEVER)

		$guid = rtrim(base64_encode(md5(microtime())),"==");

		$sqluser = "INSERT INTO users (username, email, password, guid, ip) VALUES ('$username','$email','$password','$guid', '$ip')";

        $result = $users->query($sqluser);

		$sqluser = "SELECT * FROM users WHERE username = '$username' AND guid = '$guid'";
        $result = $users->query($sqluser);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {

					setcookie('guid', $guid, time() + (86400 * 30), "/");
					setcookie('uid', $row['id'], time() + (86400 * 30), "/");

				// show a message of success and provide a true success variable
				$data['success'] = true;
				$data['message'] = 'Signing up and Logging in, please wait.';

            }
        } else {
			$data['success'] = false;
			$errors['username'] = '';
			$errors['email'] = '';
			$errors['password'] = '';
			$data['errors']  = $errors;
			$data['message'] = 'The details you have entered are not correct please check and try again! '.$result->num_rows;
        }


	}

	// return all our data to an AJAX call
	echo json_encode($data);