<?php

    // Page Settings
    $title = 'Upgrade Membership - Freelance Hunter';
    $description = 'Here you can view and bid on the latest projects.';

    // Style Sheets
    $bootstrapcss = '1';
    $metisMenucss = '1';
    $bootstrap_socialcss = '1';
    $sb_admin_2css = '1';
    $font_awesomecss = '1';
    $dataTablescss;
    $morriscss ;
    $timelinecss;

    // Javascript
    $jqueryjs = '1';
    $bootstrapjs = '1';
    $metisMenujs = '1';
    $sb_admin_2js = '1';
    $datatablesjs;
    $morrisjs;

    // Menus
    $topmenu = '1';
    $sidemenu = '1';

    // MySQLi
    $connectbids;
    $connectprojects;
    $connectsystem;
    $connectusers = '1';

    // Global Settings
    include('../parts/global/php/globalsettings.php');

?>
<?php include('../parts/global/php/sessioncheck.php'); ?>
<?php include('../parts/framework/header.php'); ?>
<?php include('../parts/global/php/menu.php'); ?>

        <div id="page-wrapper">

            <div class="col-lg-12">
                <h1 class="page-header">Upgrade Membership <?php echo $_SERVER['HTTP_REFERER']; ?></h1>
                <span></span>
            </div>

            <div class="row">
                <div class="col-lg-4">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            Free Membership
                        </div>
                        <div class="panel-body">
                            <p>Every user starts off with the free account.</p>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th colspan="2">Per Month</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th>Bids</th>
                                            <td>10</td>
                                        </tr>
                                        <tr>
                                            <th>Highlighted Bids</th>
                                            <td><i style="color:red;" class="fa fa-times"></i><div style="float:right;"><i class="fa fa-info-circle" data-target="#myModal" data-toggle="modal"></i></div></td>
                                        </tr>
                                  </tbody>
                                </table>
                            </div>
                            <div style="text-align:center;"><span style="font-size:30px;">&euro; Free</span><br/>per month</div>
                        </div>
                        <div class="panel-footer">
                        <fieldset disabled="">
                            <button class="btn btn-warning btn-lg btn-block" type="button">Upgrade</button>
                        </fieldset>
                        </div>
                    </div>
                </div>
                <!-- /.col-lg-4 -->
                <div class="col-lg-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Gold Membership
                        </div>
                        <div class="panel-body">
                            <p>This account is recommended for sole freelancers or for small freelance companies.</p>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th colspan="2">Per Month</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th>Bids</th>
                                            <td>100</td>
                                        </tr>
                                        <tr>
                                            <th>Highlighted Bids</th>
                                            <td><i style="color:red;" class="fa fa-times"></i><div style="float:right;"><i class="fa fa-info-circle" data-target="#myModal" data-toggle="modal"></i></div></td>
                                        </tr>
                                  </tbody>
                                </table>
                            </div>
                            <div style="text-align:center;"><span style="font-size:30px;">&euro; 25</span><br/>per month</div>
                        </div>
                        <div class="panel-footer">
                            <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=PH9UMJG8J2VHY"><button class="btn btn-primary btn-lg btn-block" type="button">Upgrade</button></a>
                        </div>
                    </div>
                </div>
                <!-- /.col-lg-4 -->
                <div class="col-lg-4">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            Platinum Membership
                        </div>
                        <div class="panel-body">
                            <p>This account is recommended for heavy duty freelancers or larger freelance companies.</p>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th colspan="2">Per Month</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th>Bids</th>
                                            <td><strong>Unlimited</strong></td>
                                        </tr>
                                        <tr>
                                            <th>Highlighted Bids</th>
                                            <td><i style="color:green;" class="fa fa-check"></i><div style="float:right;"><i class="fa fa-info-circle" data-target="#myModal" data-toggle="modal"></i></div></td>
                                        </tr>
                                  </tbody>
                                </table>
                            </div>
                            <div style="text-align:center;"><span style="font-size:30px;">&euro; 45</span><br/>per month</div>
                        </div>
                        <div class="panel-footer">
                            <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=J5SE6K9CT32DG"><button class="btn btn-success btn-lg btn-block" type="button">Upgrade</button></a>
                        </div>
                    </div>
                </div>
                <!-- /.col-lg-4 -->
            </div>


        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->


<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade" style="display: none;">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h4 id="myModalLabel" class="modal-title">Highlighted bids</h4>
        </div>
        <div class="modal-body">
        <ul>
            <li>Bid highlighting makes your bid stand out from the other bids.</li>
            <li>Highlighting is only avalible on the "Platinum Membership" package.</li>
            <li>Once you have purchased the "Platinum Membership" package every bid you make that month will have a slight green background drawing the clients eye, increasing your chances of winning the bid.</li>
        </ul>
        </div>
        <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>



<?php include('../parts/framework/footer.php'); ?>
